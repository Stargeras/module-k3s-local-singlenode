output "kubeconfig" {
  value       = module.kubernetes.kubeconfig
  description = "Entire formatted kubeconfig file"
}
output "kubernetes_auth_token" {
  value       = module.kubernetes.kubernetes_auth_token
  sensitive   = true
  description = "Service account token"
}
output "kubernetes_auth_cluster_ca_certificate" {
  value       = module.kubernetes.kubernetes_auth_cluster_ca_certificate
  sensitive   = true
  description = "PEM-encoded root certificates bundle for TLS authentication"
}
output "kubernetes_auth_host" {
  value = module.kubernetes.kubernetes_auth_host
  description = "The hostname (in form of URI) of the Kubernetes API"
}
output "kubernetes_auth_client_certificate" {
  value       = ""
  description = "PEM-encoded client certificate for TLS authentication"
}
output "kubernetes_auth_client_key" {
  value       = ""
  description = "PEM-encoded client certificate key for TLS authentication"
}