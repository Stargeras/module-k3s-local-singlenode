resource "kubernetes_service_account" "admin" {
  metadata {
    name      = "new-admin"
    namespace = "kube-system"
  }
}
resource "kubernetes_cluster_role_binding" "admin" {
  metadata {
    name = kubernetes_service_account.admin.metadata[0].name
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "cluster-admin"
  }

  subject {
    kind      = "ServiceAccount"
    name      = kubernetes_service_account.admin.metadata[0].name
    namespace = kubernetes_service_account.admin.metadata[0].namespace
  }
}

resource "kubernetes_secret" "token" {
  metadata {
    name      = "secret-sa-${kubernetes_service_account.admin.metadata[0].name}"
    namespace = kubernetes_service_account.admin.metadata[0].namespace
    annotations = {
      "kubernetes.io/service-account.name" = "${kubernetes_service_account.admin.metadata[0].name}"
    }
  }
  type = "kubernetes.io/service-account-token"
}

data "kubernetes_secret" "token" {
  metadata {
    name      = kubernetes_secret.token.metadata[0].name
    namespace = kubernetes_service_account.admin.metadata[0].namespace
  }
}