output "kubeconfig" {
  value = data.template_file.kubeconfig.rendered
}
output "kubernetes_auth_token" {
  value     = data.kubernetes_secret.token.data.token
  sensitive = true
}
output "kubernetes_auth_cluster_ca_certificate" {
  value     = data.kubernetes_secret.token.data["ca.crt"]
  sensitive = true
}
output "kubernetes_auth_host" {
  value = var.kubernetes_auth_host
}