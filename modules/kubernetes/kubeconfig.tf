data "template_file" "kubeconfig" {
  vars = {
    server                     = var.kubernetes_auth_host
    cluster_name               = "local-k3s"
    certificate_authority_data = "${base64encode(data.kubernetes_secret.token.data["ca.crt"])}"
    token                      = "${data.kubernetes_secret.token.data.token}"
  }
  template = <<EOT
apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: $${certificate_authority_data}
    server: $${server}
  name: $${cluster_name}
contexts:
- context:
    cluster: $${cluster_name}
    user: $${cluster_name}
  name: $${cluster_name}
current-context: $${cluster_name}
kind: Config
preferences: {}
users:
- name: $${cluster_name}
  user:
    token: $${token}
  EOT
}
