resource "null_resource" "install_k3s" {
  provisioner "local-exec" {
    interpreter = ["bash", "-c"]
    command     = <<-EOT
      curl -sfL https://get.k3s.io | sh -s - ${var.k3s_annotations}
    EOT
  }
}

data "local_file" "kubeconfig" {
  filename = "/etc/rancher/k3s/k3s.yaml"
  depends_on = [
    null_resource.install_k3s
  ]
}
