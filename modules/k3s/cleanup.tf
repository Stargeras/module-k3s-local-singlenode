resource "null_resource" "cleanup" {
  provisioner "local-exec" {
    interpreter = ["bash", "-c"]
    command     = "k3s-uninstall.sh"
    when        = destroy
  }
}