provider "kubernetes" {
  config_path = module.k3s.kubeconfig_path
}