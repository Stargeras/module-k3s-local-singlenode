# Sourcing example
```terraform
module "kubernetes" {
  source = "git::https://gitlab.com/Stargeras/module-k3s-local-singlenode"
}
```
<!-- BEGIN_TF_DOCS -->
## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_local"></a> [local](#provider\_local) | 2.4.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_k3s"></a> [k3s](#module\_k3s) | ./modules/k3s | n/a |
| <a name="module_kubernetes"></a> [kubernetes](#module\_kubernetes) | ./modules/kubernetes | n/a |

## Resources

| Name | Type |
|------|------|
| [local_file.kubeconfig](https://registry.terraform.io/providers/hashicorp/local/latest/docs/resources/file) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_k3s_annotations"></a> [k3s\_annotations](#input\_k3s\_annotations) | Annotations to pass into K3S installation | `string` | `"--write-kubeconfig-mode 0644 --disable servicelb --disable traefik"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_kubeconfig"></a> [kubeconfig](#output\_kubeconfig) | Entire formatted kubeconfig file |
| <a name="output_kubernetes_auth_client_certificate"></a> [kubernetes\_auth\_client\_certificate](#output\_kubernetes\_auth\_client\_certificate) | PEM-encoded client certificate for TLS authentication |
| <a name="output_kubernetes_auth_client_key"></a> [kubernetes\_auth\_client\_key](#output\_kubernetes\_auth\_client\_key) | PEM-encoded client certificate key for TLS authentication |
| <a name="output_kubernetes_auth_cluster_ca_certificate"></a> [kubernetes\_auth\_cluster\_ca\_certificate](#output\_kubernetes\_auth\_cluster\_ca\_certificate) | PEM-encoded root certificates bundle for TLS authentication |
| <a name="output_kubernetes_auth_host"></a> [kubernetes\_auth\_host](#output\_kubernetes\_auth\_host) | The hostname (in form of URI) of the Kubernetes API |
| <a name="output_kubernetes_auth_token"></a> [kubernetes\_auth\_token](#output\_kubernetes\_auth\_token) | Service account token |
<!-- END_TF_DOCS -->