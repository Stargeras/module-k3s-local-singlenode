variable "k3s_annotations" {
  default     = "--write-kubeconfig-mode 0644 --disable servicelb --disable traefik"
  description = "Annotations to pass into K3S installation"
}