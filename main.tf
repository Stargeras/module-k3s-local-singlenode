module "k3s" {
  source          = "./modules/k3s"
  k3s_annotations = var.k3s_annotations
}

module "kubernetes" {
  source               = "./modules/kubernetes"
  kubernetes_auth_host = "https://127.0.0.1:6443"
}