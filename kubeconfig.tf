resource "local_file" "kubeconfig" {
  content         = module.kubernetes.kubeconfig
  filename        = "${path.root}/kubeconfig.yaml"
  file_permission = "0644"
}